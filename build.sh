#!/bin/bash

# no windows support :)
# have fun :)

# clean the shit in case something got screwed up
rm -rf bin/*
mkdir bin
rm -rf content/obj

# build assembly
mcs src/*.cs -o+ -langversion:7.2 -platform:x64 -out:bin/gameassembly \
    -r:deps/MonoGame.Framework \
    -res:icon.bmp,Icon.bmp \
    $(mono buildsystem)

# be sure to have a copy of MonoGame.Framework.dll and put it in the deps folder

# package game content
cp deps/* bin/
mkdir bin/data
cp -r content/bin/DesktopGL/* bin/data/