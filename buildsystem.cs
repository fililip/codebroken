using System;
using System.IO;

class Program
{
    static void Main(string[] args)
    {
        foreach (string s in Directory.EnumerateFiles("content/maps"))
            Console.Write("-res:content/maps/" + Path.GetFileName(s) + ",maps/" + Path.GetFileNameWithoutExtension(s) + " ");

        foreach (string s in Directory.EnumerateFiles("content/dialogue"))
            Console.Write("-res:content/dialogue/" + Path.GetFileName(s) + ",dialogue/" + Path.GetFileNameWithoutExtension(s) + " ");
    }
}