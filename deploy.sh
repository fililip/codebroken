#!/bin/bash

cd bin; mkbundle --simple --static --library /usr/lib/libmono-native.so --library /usr/lib/libopenal.so --library /usr/lib/libSDL2.so -o game --config config --machine-config /etc/mono/4.5/machine.config -z gameassembly
rm gameassembly
rm *.dll
rm config
chmod +x game