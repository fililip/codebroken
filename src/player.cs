﻿using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Singleton
{
    public class Player : GameObject
    {
        private float speed = 250f;

        private Vector2 velocity = Vector2.Zero;

        private Stopwatch animationTimer = new Stopwatch();
        private bool moved = false;
        private int textureX = 0;
        private int textureY = 0;
        private int animationCounter = 0;
        public bool lockInput = false;

        public Player(ref ContentManager contentManager)
        {
            this.texture = contentManager.Load<Texture2D>("textures/player/main");
            this.size = new Vector2(48, 64);
            this.collider = new Rectangle(0, 0, 32, 48);
        }

        public void ResetVelocity() => velocity = new Vector2(0, 0);

        public void ProcessInput(GameTime gameTime)
        {
            ResetVelocity();
            if (lockInput) return;

            if (Input.up || Input.upAnalog)
            {
                velocity.Y = -speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                textureY = 3;
            }

            if (Input.down || Input.downAnalog)
            {
                velocity.Y = speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                textureY = 0;
            }

            if (Input.left || Input.leftAnalog)
            {
                velocity.X = -speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                textureY = 2;
            }

            if (Input.right || Input.rightAnalog)
            {
                velocity.X = speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                textureY = 1;
            }

            PositionCollider();
        }

        private void PositionCollider()
        {
            collider.X = (int)(position.X + 8);
            collider.Y = (int)(position.Y + 8);

            switch (textureY)
            {
                case 3:
                    collider.Y -= 8;
                    break;
                case 0:
                    collider.Y += 8;
                    break;
                case 2:
                    collider.X -= 8;
                    break;
                case 1:
                    collider.X += 8;
                    break;
            }
        }

        public Vector2 GetAnchorPosition()
        {
            Vector2 result = new Vector2(32, 32);

            bool up = Input.up || Input.upAnalog || textureY == 3;
            bool down = Input.down || Input.downAnalog || textureY == 0;
            bool left = Input.left || Input.leftAnalog || textureY == 2;
            bool right = Input.right || Input.rightAnalog || textureY == 1;

            if (up)
                result = new Vector2(32, 0);
            if (down)
                result = new Vector2(32, 64);
            if (left)
                result = new Vector2(0, 32);
            if (right)
                result = new Vector2(64, 32);
            if (up && left)
                result = new Vector2(0, 0);
            if (up && right)
                result = new Vector2(64, 0);
            if (down && left)
                result = new Vector2(0, 64);
            if (down && right)
                result = new Vector2(64, 64);

            return result + position - new Vector2(16, 0);
        }

        public Direction GetDirection()
        {
            Direction direction = Direction.Right;

            bool up = Input.up || Input.upAnalog || textureY == 3;
            bool down = Input.down || Input.downAnalog || textureY == 0;
            bool left = Input.left || Input.leftAnalog || textureY == 2;
            bool right = Input.right || Input.rightAnalog || textureY == 1;

            direction = Direction.None;

            if (up)
                direction = Direction.Up;
            if (down)
                direction = Direction.Down;
            if (left)
                direction = Direction.Left;
            if (right)
                direction = Direction.Right;
            if (up && left)
                direction = Direction.UpLeft;
            if (up && right)
                direction = Direction.UpRight;
            if (down && left)
                direction = Direction.DownLeft;
            if (down && right)
                direction = Direction.DownRight;

            return direction;
        }

        public bool Collide(Rectangle rect, bool collide = true)
        {
            if (collide)
            {
                if (velocity.X > 0 &&
                    position.X + size.X + velocity.X > rect.X &&
                    position.X < rect.X + rect.Width &&
                    position.Y + size.Y > rect.Y &&
                    position.Y < rect.Y + rect.Height)
                {
                    velocity.X = 0;
                    position.X = rect.X - size.X;
                }

                if (velocity.X < 0 &&
                    position.X + velocity.X < rect.X + rect.Width &&
                    position.X + size.X + velocity.X > rect.X &&
                    position.Y + size.Y > rect.Y &&
                    position.Y < rect.Y + rect.Height)
                {
                    velocity.X = 0;
                    position.X = rect.X + rect.Width;
                }

                if (velocity.Y > 0 &&
                    position.Y + size.Y + velocity.Y > rect.Y &&
                    position.Y < rect.Y + rect.Height &&
                    position.X + size.X > rect.X &&
                    position.X < rect.X + rect.Width)
                {
                    velocity.Y = 0;
                    position.Y = rect.Y - size.Y;
                }

                if (velocity.Y < 0 &&
                    position.Y + velocity.Y < rect.Y + rect.Height &&
                    position.Y + size.Y + velocity.Y > rect.Y &&
                    position.X + size.X > rect.X &&
                    position.X < rect.X + rect.Width)
                {
                    velocity.Y = 0;
                    position.Y = rect.Y + rect.Height;
                }
            }

            if (collider.X + collider.Width >= rect.X &&
                collider.X <= rect.X + rect.Width &&
                collider.Y + collider.Height >= rect.Y &&
                collider.Y <= rect.Y + rect.Height)
                return true;
            else
                return false;
        }

        public override void Update(GameTime gameTime)
        {
            if (lockInput) ResetVelocity();
            
            position += velocity;

            PositionCollider();

            bool moving = (velocity.X != 0 || velocity.Y != 0);

            if (moving != moved)
            {
                if (moving)
                {
                    animationTimer.Reset();
                    animationTimer.Start();
                    animationCounter = 1;
                } else
                {
                    animationTimer.Reset();
                    animationTimer.Stop();
                    animationCounter = 0;
                }

                moved = moving;
            }

            if (moving)
            {
                if (animationTimer.ElapsedMilliseconds >= 100)
                {
                    animationCounter++;
                    if (animationCounter > 3) animationCounter = 0;
                    animationTimer.Restart();
                }
            }

            switch (animationCounter)
            {
                case 0:
                    textureX = 0;
                    break;
                case 1:
                    textureX = 1;
                    break;
                case 2:
                    textureX = 0;
                    break;
                case 3:
                    textureX = 2;
                    break;
            }
        }

        public override void Draw(ref SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, new Rectangle((int)position.X - 8, (int)position.Y, 64, 64),
                             new Rectangle(textureX * 32, textureY * 32, 32, 32), Color.White);
            /* spriteBatch.Draw(Textures.white,
                             new Rectangle((int)position.X, (int)position.Y, 48, 64),
                             null, new Color(1, 1, 1, 0.4f)); */
        }
    }
}