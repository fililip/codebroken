using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Singleton
{
    namespace UI
    {
        public abstract class Element
        {
            public bool update = true;
            public Vector2 position = Vector2.Zero;
            public Vector2 size = new Vector2(32, 32);
            public Color color = Color.White;
            public float rotation = 0;
            public string ID = "";

            private Dictionary<string, object> attributes = new Dictionary<string, object>();

            public T GetAttribute<T>(string ID)
            {
                return (T)attributes[ID];
            }
            public void SetAttribute(string ID, object value) => attributes[ID] = value;
            public void RemoveAttribute(string ID) => attributes.Remove(ID);

            public virtual void Update(GameTime gameTime) {}
            public abstract void Draw(ref SpriteBatch spriteBatch);
        }

        public class MenuOption : Element
        {
            public enum Next { Layer, Action }

            public Text text;
            public Next nextType = Next.Action;
            public int nextLayer = 0;
            public Action nextAction = () => {};

            public MenuOption() => text = new Text();
            public MenuOption(ref SpriteFont font) => text = new Text(ref font);

            public void Update(string text, GameTime gameTime) => this.text.text = text;

            public override void Draw(ref SpriteBatch spriteBatch) => text.Draw(ref spriteBatch);
        }

        public class MenuLayer : Element
        {
            private float lineHeight = 0;
            private List<MenuOption> options = new List<MenuOption>();
            private bool canDraw = false;
            public int selection = 0;

            public MenuLayer(ref SpriteFont font, List<MenuOption> options)
            {
                this.options = options;
                lineHeight = font.MeasureString("A").Y;
            }

            public MenuOption.Next GetNext() { return options[selection].nextType; }

            public int GetNextLayer() { return options[selection].nextLayer; }

            public void NextAction() => options[selection].nextAction();

            public void Update(Vector2 position, GameTime gameTime)
            {
                selection = MathHelper.Clamp(selection, 0, options.Count - 1);
                for (int i = 0; i < options.Count; i++)
                {
                    Text text = options[i].text;
                    text.position = position + new Vector2(0, i * (lineHeight + 1));
                    text.color = new Color(1, 1, 1, 0.2f);
                    if (selection == i)
                    {
                        text.color.A = 255;
                        text.position.X += 8;
                    }
                }
                canDraw = true;
            }

            public override void Draw(ref SpriteBatch spriteBatch)
            {
                if (canDraw)
                foreach (MenuOption option in options)
                    option.Draw(ref spriteBatch);
            }
        }

        public class Rectangle : Element
        {
            public override void Draw(ref SpriteBatch spriteBatch)
                => spriteBatch.Draw(Textures.white, new Microsoft.Xna.Framework.Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y), null, color);
        }

        public class Menu : Element
        {
            public List<MenuLayer> layers = new List<MenuLayer>();

            private bool previousUp = false;
            private bool previousDown = false;
            private bool previousAccept = false;
            private bool previousCancel = false;

            private Stack<int> layerHistory = new Stack<int>();
            private int currentLayer = 0;
            public bool active = false;

            public void ReadyInput()
            {
                previousUp = Input.up;
                previousDown = Input.down;
                previousAccept = Input.accept;
                previousCancel = Input.cancel;
            }

            public override void Update(GameTime gameTime)
            {
                if (active && layers.Count > 0)
                {
                    MenuLayer layer = layers[currentLayer];
                    
                    if (Input.up != previousUp)
                    {
                        if (Input.up) layer.selection--;
                        previousUp = Input.up;
                    }

                    if (Input.down != previousDown)
                    {
                        if (Input.down) layer.selection++;
                        previousDown = Input.down;
                    }

                    if (Input.accept != previousAccept)
                    {
                        if (Input.accept)
                        {
                            switch (layer.GetNext())
                            {
                                case MenuOption.Next.Layer:
                                    layerHistory.Push(currentLayer);
                                    currentLayer = MathHelper.Clamp(layer.GetNextLayer(), 0, layers.Count - 1);
                                    layers[currentLayer].selection = 0;
                                    break;
                                case MenuOption.Next.Action:
                                    layer.NextAction();
                                    break;
                            }
                        }
                        previousAccept = Input.accept;
                    }

                    if (Input.cancel != previousCancel)
                    {
                        if (Input.cancel && layerHistory.Count > 0)
                            currentLayer = layerHistory.Pop();
                        previousCancel = Input.cancel;
                    }
                }

                if (layers.Count > 0) layers[currentLayer].Update(position, gameTime);
            }

            public override void Draw(ref SpriteBatch spriteBatch)
            {
                if (active && layers.Count > 0)
                    layers[currentLayer].Draw(ref spriteBatch);
            }
        }

        public class Text : Element
        {
            public SpriteFont font;

            public string text = "EMPTY";

            public Text() {}
            public Text(SpriteFont font) => this.font = font;
            public Text(ref SpriteFont font) => this.font = font;

            public Vector2 GetSize() { return font.MeasureString(text); }

            public override void Draw(ref SpriteBatch spriteBatch)
                => spriteBatch.DrawString(font, text, position, color);
        }

        public class TextBlitter : Element
        {
            public struct Character
            {
                public char character;
                public Color color;
                public bool pause;
                public bool skip;
                public int timeout;
                public int shakeIntensity;

                public Character(char character = 'A', Color color = new Color(), bool pause = false, bool skip = false, int interval = 35, int shakeIntensity = 0)
                {
                    this.character = character;
                    this.color = color;
                    this.pause = pause;
                    this.skip = skip;
                    this.timeout = interval;
                    this.shakeIntensity = shakeIntensity;
                }
            }

            public struct Line
            {
                public Character[] characters;
                public Line(Character[] characters = null) => this.characters = characters;

                public int GetLength()
                {
                    return characters.Length;
                }
            }

            private Random random = new Random();
            private SpriteFont font;
            private Vector2 fontSize;
            private List<Line> lines = new List<Line>();
            public int removedLines { get; protected set; } = 0;
            public Action lineRemoved = () => {};
            private Dictionary<char, SpriteFont.Glyph> glyphSizes = new Dictionary<char, SpriteFont.Glyph>();
            private Stopwatch timer = new Stopwatch();
            private GameTime gameTime = new GameTime();
            private int timeout = 0;
            private int offset = -1;
            private bool pause = false;
            private bool previousCancel = false;
            private bool previousAccept = false;
            public bool active = false;
            public int autoacceptTime = -1;
            private Stopwatch autoacceptTimer = new Stopwatch();

            public TextBlitter(SpriteFont font)
            {
                glyphSizes = font.GetGlyphs();
                this.font = font;
                fontSize = font.MeasureString("A");
                timer.Start();
            }

            public TextBlitter(ref SpriteFont font)
            {
                glyphSizes = font.GetGlyphs();
                this.font = font;
                fontSize = font.MeasureString("A");
                timer.Start();
            }

            public void Clear()
            {
                lines.Clear();
                removedLines = 0;
            }

            public void AddLine(Line line) => lines.Add(line);

            public void AddLine(string line)
            {
                Line l = new Line();
                l.characters = new Character[line.Length];
                for (int i = 0; i < line.Length; i++)
                    l.characters[i].character = line[i];
                lines.Add(l);
            }

            public static Line BuildLine(string input)
            {
                Line line = new Line();
                line.characters = new Character[input.Length];
                for (int i = 0; i < input.Length; i++)
                    line.characters[i] = new Character(input[i], Color.White);
                return line;
            }

            private bool InnerMarkupPresent(XmlNode characterNode)
            {
                foreach (XmlNode node in characterNode)
                    if (node.Name != "#text")
                        return true;

                return false;
            }

            Color r_color = Color.White;
            bool r_pause = false;
            bool r_skip = false;
            int r_timeout = 35;
            int r_shakeIntensity = 0;

            private void RecursiveNodeLookup(XmlNodeList nodeList, ref List<Character> characters)
            {
                for (int i = 0; i < nodeList.Count; i++)
                {
                    Color l_color = r_color;
                    bool l_pause = r_pause;
                    bool l_skip = r_skip;
                    int l_timeout = r_timeout;
                    int l_shakeIntensity = r_shakeIntensity;

                    XmlNode characterNode = nodeList[i];

                    switch (characterNode.Name)
                    {
                        case "red":
                            l_color = Color.Red;
                            break;
                        case "green":
                            l_color = Color.Green;
                            break;
                        case "blue":
                            l_color = Color.Blue;
                            break;
                        case "yellow":
                            l_color = Color.Yellow;
                            break;
                        case "white":
                            l_color = Color.White;
                            break;
                        case "c":
                            XmlAttribute R = characterNode.Attributes["r"],
                                         G = characterNode.Attributes["g"],
                                         B = characterNode.Attributes["b"],
                                         A = characterNode.Attributes["a"];
                            l_color.R = R == null ? (byte)255 : Convert.ToByte(R.InnerText);
                            l_color.G = G == null ? (byte)255 : Convert.ToByte(G.InnerText);
                            l_color.B = B == null ? (byte)255 : Convert.ToByte(B.InnerText);
                            l_color.A = A == null ? (byte)255 : Convert.ToByte(A.InnerText);
                            break;
                        case "s":
                            XmlAttribute shakeIntensity = characterNode.Attributes["v"];
                            l_shakeIntensity = shakeIntensity == null ? 4 : Convert.ToInt32(shakeIntensity.InnerText);
                            break;
                        case "p":
                            l_pause = true;
                            break;
                        case "sk":
                            l_skip = true;
                            break;
                        case "t":
                            XmlAttribute timeout = characterNode.Attributes["v"];
                            l_timeout = timeout == null ? 350 : Convert.ToInt32(timeout.InnerText);
                            break;
                    }

                    if (InnerMarkupPresent(characterNode))
                    {
                        r_color = l_color;
                        r_pause = l_pause;
                        r_skip = l_skip;
                        r_timeout = l_timeout;
                        r_shakeIntensity = l_shakeIntensity;
                        RecursiveNodeLookup(characterNode.ChildNodes, ref characters);
                    } else
                    {
                        foreach (char c in characterNode.InnerText)
                        {
                            Character character = new Character();
                            character.character = c;
                            character.color = l_color;
                            character.pause = l_pause;
                            character.skip = l_skip;
                            character.timeout = l_timeout;
                            character.shakeIntensity = l_shakeIntensity;
                            characters.Add(character);
                        }
                    }
                }

                r_color = Color.White;
                r_pause = false;
                r_skip = false;
                r_timeout = 35;
                r_shakeIntensity = 0;
            }

            public void AddQueue(string name)
            {
                Clear();
                
                Assembly assembly = Assembly.GetExecutingAssembly();
                Stream stream = assembly.GetManifestResourceStream(name);
                string content = new StreamReader(stream).ReadToEnd();

                XmlDocument document = new XmlDocument();
                document.LoadXml(content);

                foreach (XmlNode node in document["Dialogue"])
                {
                    List<Character> characters = new List<Character>();

                    RecursiveNodeLookup(node.ChildNodes, ref characters);
                    
                    Line cstring = new Line(characters.ToArray());
                    AddLine(cstring);
                }
            }

            public void ReadyInput()
            {
                previousCancel = Input.cancel;
                previousAccept = Input.accept;
            }

            private void NextLine()
            {
                autoacceptTimer.Reset();
                pause = false;
                if (offset == lines[0].GetLength() - 1)
                {
                    active = lines.Count > 1;
                    lines.RemoveAt(0);
                    removedLines++;
                    lineRemoved();
                    offset = -1;
                }
            }

            public override void Update(GameTime gameTime)
            {
                this.gameTime = gameTime;
                active = lines.Count > 0;
                if (active)
                {
                    Line current = lines[0];
                    if (timer.ElapsedMilliseconds >= timeout && !pause)
                    {
                        try
                        {
                            Character character = current.characters[offset + 1];
                            timeout = character.timeout;
                            pause = character.pause;
                            
                            if (character.skip || character.character == ' ')
                                offset++;
                        } catch { timeout = 35; }
                        offset = Math.Min(offset + 1, lines[0].GetLength() - 1);
                        timer.Restart();
                    }

                    if (offset == current.GetLength() - 1 && autoacceptTime <= -1) pause = true;

                    if (previousCancel != Input.cancel)
                    {
                        if (Input.cancel)
                        {
                            offset = lines[0].GetLength() - 1;
                            autoacceptTimer.Reset();
                        }
                        previousCancel = Input.cancel;
                    }

                    if (previousAccept != Input.accept)
                    {
                        if (Input.accept && active && autoacceptTime <= -1) NextLine();
                        previousAccept = Input.accept;
                    }

                    if (autoacceptTime > -1)
                    {
                        if (offset == current.GetLength() - 1 && !autoacceptTimer.IsRunning) autoacceptTimer.Start();
                        if (autoacceptTimer.ElapsedMilliseconds >= autoacceptTime) NextLine();
                    }
                }
            }

            public override void Draw(ref SpriteBatch spriteBatch)
            {
                if (active)
                {
                    if (size != Vector2.Zero)
                    {
                        spriteBatch.Draw(Textures.white, new Microsoft.Xna.Framework.Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y), null, Color.White);
                        spriteBatch.Draw(Textures.white, new Microsoft.Xna.Framework.Rectangle((int)position.X + 4, (int)position.Y + 4, (int)size.X - 8, (int)size.Y - 8), null, Color.Black);
                    }
                    int X = 0;
                    int Y = 0;
                    int last = 0;
                    Line current = lines[0];
                    for (int i = 0; i <= offset; i++)
                    {
                        Vector2 pos = position;

                        for (int j = last; j < i; j++)
                            if (current.characters[j].character != '\n')
                                pos.X += glyphSizes[current.characters[j].character].Cropping.Width;

                        pos.Y += Y * fontSize.Y;

                        Color drawColor = current.characters[i].color;
                        if (drawColor == Color.Transparent) drawColor = Color.White;

                        int intensity = current.characters[i].shakeIntensity;
                        Vector2 shakePos = new Vector2(random.Next(-intensity, intensity), random.Next(-intensity, intensity));

                        spriteBatch.DrawString(font, current.characters[i].character.ToString(), pos + new Vector2(16, 8) + shakePos, drawColor);
                        X++;

                        if (current.characters[i].character == '\n')
                        {
                            X = 0;
                            last = i + 1;
                            Y++;
                        }
                    }

                    if (pause && update && size != Vector2.Zero)
                        spriteBatch.DrawString(font, ">>", position + size - new Vector2(28, 38),
                            new Color(1, 1, 1, (int)gameTime.TotalGameTime.TotalMilliseconds % 500 > 249 ? 1f : 0f));
                }
            }
        }

        public class Canvas
        {
            private GraphicsDeviceManager graphics;
            private SpriteBatch spriteBatch;
            private List<Element> elements = new List<Element>();

            public Canvas(ref GraphicsDeviceManager graphics, ref SpriteBatch spriteBatch)
            {
                this.graphics = graphics;
                this.spriteBatch = spriteBatch;
            }

            public void AddElement(Element element) => elements.Add(element);

            public Element GetElement(string ID)
            {
                return elements.Find(e =>
                {
                    if (e.ID == ID) return true;
                    else return false;
                });
            }

            public List<Element> GetElements(string ID)
            {
                return elements.FindAll(e =>
                {
                    if (e.ID == ID) return true;
                    else return false;
                });
            }

            public void Update(GameTime gameTime)
            {
                foreach (Element element in elements)
                    if (element.update) element.Update(gameTime);
            }

            public void Draw()
            {
                spriteBatch.Begin(blendState: BlendState.NonPremultiplied, samplerState: SamplerState.PointClamp);
                foreach (Element element in elements)
                    element.Draw(ref spriteBatch);
                spriteBatch.End();
            }
        }
    }
}