using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Singleton
{
    public abstract class Scene
    {
        private bool firstRun = true;
        public int sceneID = -1;
        
        public GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;
        public ContentManager contentManager;

        public Scene(ref GraphicsDeviceManager graphics, ref SpriteBatch spriteBatch, ref ContentManager contentManager)
        {
            this.graphics = graphics;
            this.spriteBatch = spriteBatch;
            this.contentManager = contentManager;
        }

        protected virtual void Start() {}
        
        protected void BeforeUpdate()
        {
            if (firstRun)
            {
                Start();
                firstRun = false;
            }
        }

        public abstract int Update(GameTime gameTime);

        public virtual void SwitchScene(int ID)
        {
            BeforeQuit();
            sceneID = ID;
        }

        protected virtual void BeforeQuit() => firstRun = true;

        protected virtual void Exit() => sceneID = -1;

        public abstract void Draw();
    }

    namespace Scenes
    {
        public class Disclaimer : Scene
        {
            private SpriteFont font;
            private UI.Canvas canvas;
            private UI.Text warningTop;
            private UI.Text warningBottom;

            public Disclaimer(ref GraphicsDeviceManager graphics, ref SpriteBatch spriteBatch, ref ContentManager contentManager)
                : base(ref graphics, ref spriteBatch, ref contentManager)
            {
                sceneID = 0;

                font = contentManager.Load<SpriteFont>("fonts/regular");
                canvas = new UI.Canvas(ref graphics, ref spriteBatch);

                warningTop = new UI.Text(ref font);
                warningTop.text = "this game contains explicit content";
                warningBottom = new UI.Text(ref font);
                warningBottom.text = "proceed at own discretion";

                warningTop.position = new Vector2(640 / 2 - warningTop.GetSize().X / 2, 480 / 2 - warningBottom.GetSize().Y / 2);
                warningBottom.position = new Vector2(640 / 2 - warningBottom.GetSize().X / 2, 480 / 2 + warningBottom.GetSize().Y / 2);
                warningTop.color.A = 0;
                warningBottom.color.A = 0;

                canvas.AddElement(warningTop);
                canvas.AddElement(warningBottom);
            }

            protected override void Start()
            {
                new Task(() =>
                {
                    Thread.Sleep(1000);
                    new Task(() =>
                    {
                        while (warningTop.color.A < 250)
                        {
                            warningTop.color.A += 50;
                            Thread.Sleep(250);
                        }
                    }).Start();
                    Thread.Sleep(2500);
                    new Task(() =>
                    {
                        while (warningBottom.color.A < 250)
                        {
                            warningBottom.color.A += 50;
                            Thread.Sleep(250);
                        }
                        Thread.Sleep(1500);
                        warningTop.color.A = warningBottom.color.A = 0;
                        Thread.Sleep(1000);
                        SwitchScene(3);
                    }).Start();
                }).Start();
            }

            public override int Update(GameTime gameTime)
            {
                BeforeUpdate();
                if (Input.accept) sceneID = 3;
                canvas.Update(gameTime);
                return sceneID;
            }

            public override void Draw()
            {
                canvas.Draw();
            }
        }

        public class MainMenu : Scene
        {
            private SpriteFont font;
            private UI.Canvas canvas;
            private UI.Menu menu = new UI.Menu();

            public MainMenu(ref GraphicsDeviceManager graphics, ref SpriteBatch spriteBatch, ref ContentManager contentManager)
                : base(ref graphics, ref spriteBatch, ref contentManager)
            {
                sceneID = 1;

                font = contentManager.Load<SpriteFont>("fonts/regular");
                canvas = new UI.Canvas(ref graphics, ref spriteBatch);

                List<UI.MenuLayer> menuLayers = new List<UI.MenuLayer>();
                List<UI.MenuOption> mainLayer = new List<UI.MenuOption>();

                {
                    UI.MenuOption play = new UI.MenuOption(ref font);
                    play.text.text = "play";
                    play.nextAction = () => SwitchScene(sceneID + 1);
                    mainLayer.Add(play);

                    UI.MenuOption options = new UI.MenuOption(ref font);
                    options.text.text = "options";
                    options.nextType = UI.MenuOption.Next.Layer;
                    options.nextLayer = 1;
                    mainLayer.Add(options);

                    UI.MenuOption credits = new UI.MenuOption(ref font);
                    credits.text.text = "credits";
                    credits.nextType = UI.MenuOption.Next.Layer;
                    credits.nextLayer = 2;
                    mainLayer.Add(credits);

                    UI.MenuOption quit = new UI.MenuOption(ref font);
                    quit.text.text = "quit";
                    quit.nextType = UI.MenuOption.Next.Action;
                    quit.nextAction = Exit;
                    mainLayer.Add(quit);
                }

                List<UI.MenuOption> optionsLayer = new List<UI.MenuOption>();

                {
                    UI.MenuOption fullscreen = new UI.MenuOption(ref font);
                    fullscreen.text.text = graphics.IsFullScreen ? "fullscreen" : "windowed";
                    fullscreen.SetAttribute("state", graphics.IsFullScreen);
                    fullscreen.nextAction = () =>
                    {
                        bool newValue = !fullscreen.GetAttribute<bool>("state");
                        fullscreen.SetAttribute("state", newValue);

                        if (newValue)
                            fullscreen.text.text = "fullscreen";
                        else
                            fullscreen.text.text = "windowed";
                        
                        Program.gameInstance.graphics.IsFullScreen = newValue;
                        Program.gameInstance.graphics.ApplyChanges();
                        Program.gameInstance.configFile.SetIntProperty("fullscreen", Convert.ToInt32(newValue));
                    };
                    optionsLayer.Add(fullscreen);

                    UI.MenuOption option2 = new UI.MenuOption(ref font);
                    option2.text.text = "option 2";
                    optionsLayer.Add(option2);
                }

                List<UI.MenuOption> creditsLayer = new List<UI.MenuOption>();

                {
                    UI.MenuOption fililip = new UI.MenuOption(ref font);
                    fililip.text.text = "fililip - code";
                    creditsLayer.Add(fililip);

                    UI.MenuOption pimpek32 = new UI.MenuOption(ref font);
                    pimpek32.text.text = "pimpek32 - assets";
                    creditsLayer.Add(pimpek32);
                }

                menuLayers.Add(new UI.MenuLayer(ref font, mainLayer));
                menuLayers.Add(new UI.MenuLayer(ref font, optionsLayer));
                menuLayers.Add(new UI.MenuLayer(ref font, creditsLayer));

                menu.layers = menuLayers;
                menu.position = new Vector2(16, 8);
                menu.active = true;
                canvas.AddElement(menu);
            }

            protected override void Start()
            {
                sceneID = 1;
                menu.ReadyInput();
            }

            public override int Update(GameTime gameTime)
            {
                BeforeUpdate();
                canvas.Update(gameTime);
                return sceneID;
            }

            public override void Draw()
            {
                canvas.Draw();
            }
        }

        public class Game : Scene
        {
            public Map map = null;
            public Camera camera = new Camera();
            public UI.Canvas canvas;
            public UI.TextBlitter textBlitter;
            public SpriteFont font;
            public UI.Rectangle rectangle = new UI.Rectangle();
            public UI.Menu pauseMenu;
            public Player player;
            public bool previousCancel = false;

            public Game(ref GraphicsDeviceManager graphics, ref SpriteBatch spriteBatch, ref ContentManager contentManager)
                : base(ref graphics, ref spriteBatch, ref contentManager)
            {
                sceneID = 2;
                
                GraphicsDevice graphicsDevice = graphics.GraphicsDevice;
                map = new Map(ref graphicsDevice);
                player = new Player(ref contentManager);
                player.visible = false;
                canvas = new UI.Canvas(ref graphics, ref spriteBatch);
                textBlitter = new UI.TextBlitter(contentManager.Load<SpriteFont>("fonts/large"));
                textBlitter.position = new Vector2(40, 324);
                textBlitter.size = new Vector2(560, 124);
                canvas.AddElement(textBlitter);

                rectangle.size = new Vector2(640, 480);
                rectangle.color.R = rectangle.color.G = rectangle.color.B = rectangle.color.A = 0;
                canvas.AddElement(rectangle);

                font = contentManager.Load<SpriteFont>("fonts/regular");
                pauseMenu = new UI.Menu();
                pauseMenu.position = new Vector2(16, 8);
                UI.MenuOption unpause = new UI.MenuOption(ref font);
                unpause.text.text = "back to game";
                unpause.nextAction = () =>
                {
                    textBlitter.ReadyInput();
                    map.ReadyInput();
                    pauseMenu.active = false;
                };
                UI.MenuOption save = new UI.MenuOption(ref font);
                save.text.text = "save";
                save.nextAction = () =>
                {
                    Program.gameInstance.saveIndicator.Show();
                    Program.gameInstance.saveFile.SaveToFile("save");
                };
                UI.MenuOption quit = new UI.MenuOption(ref font);
                quit.text.text = "quit to main menu";
                quit.nextAction = () =>
                {
                    pauseMenu.active = false;
                    pauseMenu.layers[0].selection = 0;
                    SwitchScene(sceneID - 1);
                };
                UI.MenuLayer layer = new UI.MenuLayer(ref font, new List<UI.MenuOption>() { unpause, save, quit });
                pauseMenu.layers.Add(layer);
                canvas.AddElement(pauseMenu);

                camera.smoothing = 0.15f;
            }

            protected override void Start()
            {
                sceneID = 2;
                pauseMenu.ReadyInput();
                textBlitter.ReadyInput();
                map.Load("maps/intro", player);
                previousCancel = Input.cancel;
            }

            public override int Update(GameTime gameTime)
            {
                BeforeUpdate();

                if (previousCancel != Input.escape)
                {
                    if (Input.escape) pauseMenu.active = true;
                    previousCancel = Input.escape;
                }

                textBlitter.update = !pauseMenu.active;
                
                if (!pauseMenu.active)
                {
                    rectangle.color.A = 0;
                    if (!textBlitter.active)
                        player.ProcessInput(gameTime);

                    map.Update(gameTime, this, ref player, !textBlitter.active);
                    player.Update(gameTime);

                    camera.position = new Vector2
                    (
                        MathHelper.Clamp(-player.position.X + SingletonGame.WIDTH / 2 - 24, -map.size.X + SingletonGame.WIDTH, 0),
                        MathHelper.Clamp(-player.position.Y + SingletonGame.HEIGHT / 2 - 32, -map.size.Y + SingletonGame.HEIGHT, 0)
                    );
                    camera.Update(gameTime);
                } else
                    rectangle.color.A = 200;
                canvas.Update(gameTime);

                return sceneID;
            }

            public override void Draw()
            {
                spriteBatch.Begin(blendState: BlendState.NonPremultiplied, samplerState: SamplerState.PointClamp, transformMatrix: camera.matrix);
                map.Draw(ref spriteBatch);
                player.Draw(ref spriteBatch);
                spriteBatch.End();
                canvas.Draw();
            }
        }
    
        public class Cutscene : Scene
        {
            class Image
            {
                public float opacity;
                public Texture2D texture;

                public Image(Texture2D texture = null, float opacity = 0f)
                {
                    this.texture = texture;
                    this.opacity = opacity;
                }
            }

            private UI.Canvas canvas;
            private UI.TextBlitter textBlitter;
            private List<Image> images = new List<Image>();
            private int currentImage = 0;
            private bool lastImageShown = false;
            private Task showTask;
            private bool canContinue = false;

            public Cutscene(ref GraphicsDeviceManager graphics, ref SpriteBatch spriteBatch, ref ContentManager contentManager)
                : base(ref graphics, ref spriteBatch, ref contentManager)
            {
                sceneID = 3;

                canvas = new UI.Canvas(ref graphics, ref spriteBatch);
                textBlitter = new UI.TextBlitter(contentManager.Load<SpriteFont>("fonts/large"));
                textBlitter.size = Vector2.Zero;
                textBlitter.position.Y = 350;
                textBlitter.autoacceptTime = 1500;
                textBlitter.lineRemoved = () =>
                {
                    canContinue = true;
                    if (textBlitter.removedLines == 2)
                    {
                        new Task(() =>
                        {
                            Thread.Sleep(1800);
                            canContinue = true;
                        }).Start();
                    }
                };
                canvas.AddElement(textBlitter);
            }

            protected override void Start()
            {
                sceneID = 3;
                lastImageShown = false;
                images.Clear();
                images.Add(new Image(contentManager.Load<Texture2D>("textures/rooms/room_1")));
                images.Add(new Image(contentManager.Load<Texture2D>("textures/player/comes")));
                images.Add(new Image(contentManager.Load<Texture2D>("textures/player/main")));
                showTask = new Task(() =>
                {
                    while (true)
                    {
                        float delta = 0.05f;
                        int waitMS = 50;

                        if (currentImage < images.Count)
                            while (images[currentImage].opacity < 0.95f)
                            {
                                if (currentImage > 0) images[currentImage - 1].opacity -= delta;
                                images[currentImage].opacity += delta;
                                Thread.Sleep(waitMS);
                            }
                        else
                        {
                            while (images[currentImage - 1].opacity > 0.05f)
                            {
                                images[currentImage - 1].opacity -= delta;
                                Thread.Sleep(waitMS);
                            }
                            lastImageShown = true;
                        }

                        while (!canContinue) Thread.Sleep(1);
                        canContinue = false;
                        currentImage++;
                    }
                });
                showTask.Start();
                textBlitter.AddQueue("dialogue/intro_window");
            }

            public override int Update(GameTime gameTime)
            {
                BeforeUpdate();
                canvas.Update(gameTime);
                if (lastImageShown) SwitchScene(2);
                return sceneID;
            }

            public override void Draw()
            {
                spriteBatch.Begin(blendState: BlendState.NonPremultiplied, samplerState: SamplerState.PointClamp);
                
                foreach (Image image in images)
                    if (image.opacity > 0.05f)
                        spriteBatch.Draw(
                            image.texture, new Vector2(
                                SingletonGame.WIDTH / 2f - image.texture.Width / 2f,
                                SingletonGame.HEIGHT / 2f - image.texture.Height / 2f - 64f
                            ), null,
                            new Color(1f, 1f, 1f, image.opacity),
                            0, Vector2.Zero, 1f,
                            SpriteEffects.None, 0
                        );

                spriteBatch.End();
                canvas.Draw();
            }
        }
    }
}