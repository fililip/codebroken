using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Singleton
{
    public class Chunk
    {
        public Vector2 position = Vector2.Zero;
        private List<GameObject> gameObjects = new List<GameObject>();

        public void Load(ref GraphicsDevice graphicsDevice, string name)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            Stream stream = assembly.GetManifestResourceStream(name);
            Texture2D texture = Texture2D.FromStream(graphicsDevice, stream);
            
            Color[] pixels = new Color[texture.Width * texture.Height];
            texture.GetData(pixels);

            for (int y = 0; y < texture.Height; y++)
            for (int x = 0; x < texture.Width; x++)
            {
                Color pixel = pixels[x + y * texture.Width];

                GameObject gameObject = new GameObject();
                gameObject.position = new Vector2(x * 32, y * 32);
                gameObject.behavior = new EmptyBehavior();

                switch (pixel.R)
                {
                    case 0:
                        /* TextResponseBehavior behavior = new TextResponseBehavior();
                        behavior.wall = true;
                        gameObject.behavior = behavior; */
                        break;
                }

                if (pixel != Color.Transparent) AddObject(gameObject);
            }
        }

        public void ReadyInput()
        {
            foreach (GameObject gameObject in gameObjects)
                gameObject.ReadyInput();
        }

        public void AddObject(GameObject gameObject)
        {
            gameObject.position += position;
            gameObjects.Add(gameObject);
        }

        public void Update(GameTime gameTime, Scene scene, ref Player player, bool acceptKeyEvent = true)
        {
            bool collided = false;
            for (int i = 0; i < gameObjects.Count; i++)
            {
                GameObject gameObject = gameObjects[i];

                if (gameObject.behavior != null)
                {
                    bool collision = player.Collide(new Rectangle((int)gameObject.position.X, (int)gameObject.position.Y,
                        (int)gameObject.size.X, (int)gameObject.size.Y), gameObject.collidable);
                    if (collision && !collided)
                    {
                        gameObject.behavior.collision = true;
                        collided = true;
                    } else gameObject.behavior.collision = false;
                    gameObject.behavior.Loop(gameTime, gameObject, scene, acceptKeyEvent);
                    if (!gameObject.behavior.active)
                    {
                        gameObjects.RemoveAt(i);
                        i--;
                    }
                } else
                    player.Collide(new Rectangle((int)gameObject.position.X,
                        (int)gameObject.position.Y, (int)gameObject.size.X, (int)gameObject.size.Y));
            }
        }

        public void Draw(ref SpriteBatch spriteBatch)
        {
            foreach (GameObject gameObject in gameObjects)
            {
                if (gameObject.visible)
                    gameObject.Draw(ref spriteBatch);
            }
        }
    }

    public class Map
    {
        const int blockSize = 32;
        const int chunkSize = 16;
        private Chunk[,] chunks = new Chunk[1, 1];
        private uint[] chunkRangeStart = new uint[2];
        private uint[] chunkRangeStop = new uint[2];
        public string name { get; protected set; } = "";
        public Vector2 size { get; protected set; } = Vector2.Zero;
        private GraphicsDevice graphicsDevice = null;

        public Map(ref GraphicsDevice graphicsDevice) => this.graphicsDevice = graphicsDevice;

        public void Load(string name, Player player = null)
        {
            this.name = name;
            chunks = new Chunk[1, 1];
            GC.Collect();

            Assembly assembly = Assembly.GetExecutingAssembly();

            Stream stream = assembly.GetManifestResourceStream(name);
            Texture2D texture = Texture2D.FromStream(graphicsDevice, stream);
            
            size = new Vector2(texture.Width, texture.Height) * blockSize;

            Color[] pixels = new Color[texture.Width * texture.Height];
            texture.GetData(pixels);

            for (uint i = 0; i < Math.Ceiling(texture.Height / (double)chunkSize); i++)
            for (uint j = 0; j < Math.Ceiling(texture.Width / (double)chunkSize); j++)
            {
                Chunk chunk = new Chunk();

                chunk.position.X = j * chunkSize * blockSize;
                chunk.position.Y = i * chunkSize * blockSize;

                for (uint y = 0; y < chunkSize; y++)
                for (uint x = 0; x < chunkSize; x++)
                {
                    uint index = (x + (uint)(chunk.position.X / blockSize)) + (y + (uint)(chunk.position.Y / blockSize)) * (uint)(texture.Width);
                    if (index > pixels.Length - 1 || index < 0) continue;
                    Color pixel = pixels[index];

                    GameObject gameObject = new GameObject();
                    gameObject.position = new Vector2(x * blockSize, y * blockSize);
                    gameObject.behavior = new EmptyBehavior();

                    switch (pixel.R)
                    {
                        case 20:
                            gameObject.behavior = new BedBehavior();
                            gameObject.collidable = false;
                            gameObject.size = new Vector2(128, 64);
                            break;
                        case 60:
                            gameObject.behavior = new WindowBehavior();
                            gameObject.collidable = false;
                            gameObject.size.Y = 64;
                            break;
                        case 80:
                            gameObject.behavior = new WindowBehavior();
                            gameObject.collidable = false;
                            gameObject.size.Y = 64;
                            break;
                    }

                    switch (pixel.G)
                    {
                        case 255:
                            if (pixel.R == 255 || pixel.B == 255) break;
                            gameObject.collidable = false;
                            gameObject.visible = false;
                            if (player != null)
                            {
                                player.position = gameObject.position;
                                player.visible = true;
                            }
                            break;
                    }

                    if (pixel != Color.Transparent) chunk.AddObject(gameObject);
                }

                AddChunk(j, i, chunk);
            }
        }

        public void ReadyInput()
        {
            foreach (Chunk chunk in chunks)
                chunk.ReadyInput();
        }

        public void AddChunk(uint x, uint y, Chunk chunk)
        {
            if (x < chunks.GetLength(0) && y < chunks.GetLength(1))
                chunks[x, y] = chunk;
            else
            {
                uint newX = (uint)chunks.GetLength(0);
                uint newY = (uint)chunks.GetLength(1);

                if (x >= chunks.GetLength(0)) newX = x + 1;
                if (y >= chunks.GetLength(1)) newY = y + 1;

                Chunk[,] newChunks = new Chunk[newX, newY];
                
                for (uint yy = 0; yy < chunks.GetLength(1); yy++)
                for (uint xx = 0; xx < chunks.GetLength(0); xx++)
                    newChunks[xx, yy] = chunks[xx, yy];

                newChunks[x, y] = chunk;
                chunks = newChunks;
            }
        }

        private void CalculateChunkRanges(ref Player player)
        {
            chunkRangeStart[0] = (uint)MathHelper.Clamp(player.position.X / (chunkSize * blockSize) - 1, 0, chunks.GetLength(0) - 1);
            chunkRangeStop[0] = (uint)MathHelper.Min(chunkRangeStart[0] + 3, chunks.GetLength(0));

            chunkRangeStart[1] = (uint)MathHelper.Clamp(player.position.Y / (chunkSize * blockSize) - 1, 0, chunks.GetLength(1) - 1);
            chunkRangeStop[1] = (uint)MathHelper.Min(chunkRangeStart[1] + 3, chunks.GetLength(1));
        }

        public void Update(GameTime gameTime, Scene scene, ref Player player, bool acceptKeyEvent = true)
        {
            CalculateChunkRanges(ref player);
            for (uint y = chunkRangeStart[1]; y < chunkRangeStop[1]; y++)
            for (uint x = chunkRangeStart[0]; x < chunkRangeStop[0]; x++)
                chunks[x, y].Update(gameTime, scene, ref player, acceptKeyEvent && !player.lockInput);
            
            int sizeX = (chunks.GetLength(0)) * chunkSize * blockSize;
            int sizeY = (chunks.GetLength(1)) * chunkSize * blockSize;

            player.Collide(new Rectangle(0, -32, sizeX, 32));
            player.Collide(new Rectangle(-32, 0, 32, sizeY));
            player.Collide(new Rectangle(sizeX, 0, 32, sizeY));
            player.Collide(new Rectangle(0, sizeY, sizeX, 32));
        }

        public void Draw(ref SpriteBatch spriteBatch)
        {
            //int lineSize = 4;
            for (uint y = chunkRangeStart[1]; y < chunkRangeStop[1]; y++)
            for (uint x = chunkRangeStart[0]; x < chunkRangeStop[0]; x++)
            {
                Chunk chunk = chunks[x, y];
                chunk.Draw(ref spriteBatch);
                /* // top
                spriteBatch.Draw(Textures.white,
                    new Rectangle((int)chunk.position.X, (int)chunk.position.Y, blockSize * chunkSize, lineSize),
                    null, Color.Red);
                // right
                spriteBatch.Draw(Textures.white,
                    new Rectangle((int)chunk.position.X + blockSize * chunkSize, (int)chunk.position.Y, lineSize, blockSize * chunkSize + lineSize),
                    null, Color.Red);
                // bottom
                spriteBatch.Draw(Textures.white,
                    new Rectangle((int)chunk.position.X, (int)chunk.position.Y + blockSize * chunkSize, blockSize * chunkSize + lineSize, lineSize),
                    null, Color.Red);
                // left
                spriteBatch.Draw(Textures.white,
                    new Rectangle((int)chunk.position.X, (int)chunk.position.Y, lineSize, blockSize * chunkSize),
                    null, Color.Red); */
            }
        }
    }

    public class ChunklessMap
    {
        private List<GameObject> gameObjects = new List<GameObject>();

        public void Load(ref GraphicsDevice graphicsDevice, string name)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            Stream stream = assembly.GetManifestResourceStream(name);
            Texture2D texture = Texture2D.FromStream(graphicsDevice, stream);
            
            Color[] pixels = new Color[texture.Width * texture.Height];
            texture.GetData(pixels);

            for (int y = 0; y < texture.Height; y++)
            for (int x = 0; x < texture.Width; x++)
            {
                Color pixel = pixels[x + y * texture.Width];

                GameObject gameObject = new GameObject();
                gameObject.position = new Vector2(x * 32, y * 32);
                gameObject.behavior = new EmptyBehavior();

                switch (pixel.R)
                {
                    case 0:
                        /* TextResponseBehavior behavior = new TextResponseBehavior();
                        behavior.wall = true;
                        gameObject.behavior = behavior; */
                        break;
                }

                if (pixel != Color.Transparent) AddObject(gameObject);
            }
        }

        public void ReadyInput()
        {
            foreach (GameObject gameObject in gameObjects)
                gameObject.ReadyInput();
        }

        public void AddObject(GameObject gameObject) => gameObjects.Add(gameObject);

        public void Update(GameTime gameTime, Scene scene, ref Player player, bool acceptKeyEvent = true)
        {
            Direction direction = player.GetDirection();

            bool collided = false;
            for (int i = 0; i < gameObjects.Count; i++)
            {
                GameObject gameObject = gameObjects[i];

                if (gameObject.behavior != null)
                {
                    bool collision = player.Collide(new Rectangle((int)gameObject.position.X, (int)gameObject.position.Y,
                        (int)gameObject.size.X, (int)gameObject.size.Y), gameObject.collidable);
                    if (collision && !collided)
                    {
                        gameObject.behavior.collision = true;
                        collided = true;
                    } else gameObject.behavior.collision = false;
                    gameObject.behavior.Loop(gameTime, gameObject, scene, acceptKeyEvent);
                    if (!gameObject.behavior.active)
                    {
                        gameObjects.RemoveAt(i);
                        i--;
                    }
                } else
                    player.Collide(new Rectangle((int)gameObject.position.X,
                        (int)gameObject.position.Y, (int)gameObject.size.X, (int)gameObject.size.Y));
            }
        }

        public void Draw(ref SpriteBatch spriteBatch)
        {
            foreach (GameObject gameObject in gameObjects)
            {
                if (gameObject.visible)
                    gameObject.Draw(ref spriteBatch);
            }
        }
    }
}