using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Singleton
{
    public abstract class Behavior
    {
        protected GameObject self;
        protected Scene scene;
        public bool collision = false;
        public bool active { get; protected set; } = true;
        protected bool pressed = false;

        protected abstract void OnInteraction();

        public void ReadyInput() => pressed = Input.accept;

        public virtual void Loop(GameTime gameTime, GameObject self, Scene scene, bool acceptKeyEvent = true)
        {
            this.self = self;
            this.scene = scene;
            if (!Input.accept) pressed = false;
            if (!pressed && Input.accept)
            {
                pressed = true;

                if (collision && acceptKeyEvent)
                    OnInteraction();

                new Task(() =>
                {
                    while (Input.accept)
                        Thread.Sleep(1);

                    pressed = false;
                }).Start();
            }
        }
    }

    public class EmptyBehavior : Behavior
    {
        protected override void OnInteraction()
        {
            Scenes.Game scene = (Scenes.Game)this.scene;
        }

        public override void Loop(GameTime gameTime, GameObject self, Scene scene, bool acceptKeyEvent = true)
        {
            base.Loop(gameTime, self, scene, acceptKeyEvent);
        }
    }

    public class BedBehavior : Behavior
    {
        protected override void OnInteraction()
        {
            Scenes.Game scene = (Scenes.Game)this.scene;
            scene.player.ResetVelocity();
            scene.textBlitter.AddQueue("dialogue/intro_bed");
        }

        public override void Loop(GameTime gameTime, GameObject self, Scene scene, bool acceptKeyEvent = true)
        {
            base.Loop(gameTime, self, scene, acceptKeyEvent);
        }
    }

    public class WindowBehavior : Behavior
    {
        Scenes.Game gameScene = null;
        Task task = null;

        protected override void OnInteraction()
        {
            gameScene = (Scenes.Game)this.scene;
            gameScene.textBlitter.AddQueue("dialogue/intro_window");
            gameScene.player.lockInput = true;
        }

        void PlayEvent()
        {
            task = new Task(() =>
            {
                // sample "cutscene"
                /* Thread.Sleep(800);
                gameScene.player.position.X = 0; */
                gameScene.player.lockInput = false;
                task = null;
            });
            task.Start();
        }

        public override void Loop(GameTime gameTime, GameObject self, Scene scene, bool acceptKeyEvent = true)
        {
            do
            {
                if (gameScene == null) break;
                if (!gameScene.player.lockInput) break;
                if (!gameScene.textBlitter.active && task == null)
                    PlayEvent();
            } while (false);
            base.Loop(gameTime, self, scene, acceptKeyEvent);
        }
    }
}