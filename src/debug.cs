using System;

namespace Singleton
{
    public class Debug
    {
        public static void Error(object o)
        {
            ConsoleColor previousColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(o);
            Console.ForegroundColor = previousColor;
        }

        public static void Warning(object o)
        {
            ConsoleColor previousColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(o);
            Console.ForegroundColor = previousColor;
        }

        public static void Log(object o)
        {
            Console.WriteLine(o);
        }
    }
}