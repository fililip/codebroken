﻿using System;

namespace Singleton
{
    static class Program
    {
        public static SingletonGame gameInstance = new SingletonGame();

        [STAThread] static void Main() => gameInstance.Run();
    }
}