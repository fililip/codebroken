using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Singleton
{
    public class SaveIndicator : GameObject
    {
        private SpriteFont font;
        private Stopwatch timer = new Stopwatch();
        private int counter = 0;
        private float opacity = 0f;
        private string saveText = "";
        private bool drawAnimation = false;
        private bool showing = false;

        public SaveIndicator(ref ContentManager contentManager)
        {
            this.texture = contentManager.Load<Texture2D>("textures/animations/loading");
            this.font = contentManager.Load<SpriteFont>("fonts/regular");
            this.size = new Vector2(32, 32);
            timer.Start();
        }

        public void Show()
        {
            if (!showing)
            {
                drawAnimation = true;
                saveText = "Saving...";
                opacity = 1f;
                new Task(() =>
                {
                    showing = true;
                    Thread.Sleep(500);
                    drawAnimation = false;
                    saveText = "Saved";
                    while (opacity > 0f)
                    {
                        opacity -= 0.025f;
                        Thread.Sleep(50);
                    }
                    showing = false;
                }).Start();
            }
        }
        
        public override void Update(GameTime gameTime)
        {
            if (timer.ElapsedMilliseconds >= 100)
            {
                counter++;
                if (counter > 5) counter = 0;
                timer.Restart();
            }
        }

        public override void Draw(ref SpriteBatch spriteBatch)
        {
            if (drawAnimation)
                spriteBatch.Draw(texture,
                                 new Rectangle((int)position.X, (int)position.Y, 32, 32),
                                 new Rectangle(8 * counter, 0, 8, 8), new Color(1, 1, 1, opacity));
            spriteBatch.DrawString(font, saveText, position + new Vector2(drawAnimation ? 38 : 0, -2),
                             new Color(1, 1, 1, opacity), 0, Vector2.Zero, 1.5f, SpriteEffects.None, 0);
        }
    }
}