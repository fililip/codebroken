﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Singleton
{
    public class SingletonGame : Game
    {
        public GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;
        public ContentManager contentManager;
        public List<Scene> scenes = new List<Scene>();
        public RenderTarget2D resizer;
        public int currentScene = 0;
        public SaveFile saveFile = new SaveFile();
        public SaveFile configFile = new SaveFile();
        public SaveIndicator saveIndicator;

        public static readonly int WIDTH = 640;
        public static readonly int HEIGHT = 480;

#if UNITY
        private double sine = 60.0;
        private double targetSine = 60.0;
        private Stopwatch sw = new Stopwatch();
#endif

        public SingletonGame()
        {
            saveFile.LoadFromFile("save");
            configFile.LoadFromFile("config");

            graphics = new GraphicsDeviceManager(this);
#if UNITY
            graphics.SynchronizeWithVerticalRetrace = false;
#else
            graphics.SynchronizeWithVerticalRetrace = true;
#endif
            graphics.GraphicsProfile = GraphicsProfile.HiDef;
            graphics.PreferredBackBufferWidth = WIDTH;
            graphics.PreferredBackBufferHeight = HEIGHT;
            graphics.HardwareModeSwitch = false;
            graphics.IsFullScreen = Convert.ToBoolean(configFile.GetIntProperty("fullscreen"));
            Debug.Warning(configFile.GetIntProperty("fullscreen"));
            graphics.ApplyChanges();

            Window.Title = " ";
            Window.AllowUserResizing = true;

            Content.RootDirectory = "data";
            contentManager = Content;
            spriteBatch = new SpriteBatch(GraphicsDevice);

            scenes.Add(new Scenes.Disclaimer(ref graphics, ref spriteBatch, ref contentManager));
            scenes.Add(new Scenes.MainMenu(ref graphics, ref spriteBatch, ref contentManager));
            scenes.Add(new Scenes.Game(ref graphics, ref spriteBatch, ref contentManager));
            scenes.Add(new Scenes.Cutscene(ref graphics, ref spriteBatch, ref contentManager));

            IsMouseVisible = true;
#if UNITY
            IsFixedTimeStep = true;
            sw.Start();
#else
            IsFixedTimeStep = false;
#endif

            GraphicsDevice graphicsDevice = GraphicsDevice;
            Textures.Initialize(ref graphicsDevice);

            resizer = new RenderTarget2D(GraphicsDevice, WIDTH, HEIGHT);
            saveIndicator = new SaveIndicator(ref contentManager);
            saveIndicator.position = new Vector2(8, 440);
        }

        protected override void Update(GameTime gameTime)
        {
            Input.Update(this);
            currentScene = scenes[currentScene].Update(gameTime);

            // unity game "engine" emulator mode ™
#if UNITY
            if (sw.ElapsedMilliseconds >= new Random().Next(150, 450))
            {
                targetSine = new Random().Next(20, 240);
                sw.Restart();
            }

            sine += 0.09 * (targetSine - sine);
            TargetElapsedTime = TimeSpan.FromSeconds(1d / sine);
#endif

            saveIndicator.Update(gameTime);

            if (currentScene == -1)
                Exit();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.SetRenderTarget(resizer);
            GraphicsDevice.Clear(Color.Transparent);
            scenes[currentScene].Draw();
            spriteBatch.Begin(samplerState: SamplerState.PointClamp, blendState: BlendState.NonPremultiplied);
            saveIndicator.Draw(ref spriteBatch);
            spriteBatch.End();
            GraphicsDevice.SetRenderTarget(null);
            GraphicsDevice.Clear(Color.Transparent);
            spriteBatch.Begin(samplerState: SamplerState.LinearClamp);

            double width;
            double height;

            if (WIDTH / (double)HEIGHT < Window.ClientBounds.Width / (double)Window.ClientBounds.Height) {
                height = Window.ClientBounds.Height;
                width = Math.Floor((double)WIDTH
                                   * Window.ClientBounds.Height / (double)HEIGHT);
            } else {
                width = Window.ClientBounds.Width;
                height = Math.Floor((double)HEIGHT
                                    * Window.ClientBounds.Width / (double)WIDTH);
            }

            spriteBatch.Draw(resizer,
                new Rectangle((int)(Window.ClientBounds.Width / 2.0 - width / 2.0),
                              (int)(Window.ClientBounds.Height / 2.0 - height / 2.0), (int)width, (int)height), Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            configFile.SaveToFile("config");
            Process.GetCurrentProcess().Kill();
        }
    }
}
