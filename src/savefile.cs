﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Singleton
{
    public class SaveFile
    {
        private Dictionary<string, object> properties = new Dictionary<string, object>();

        public bool HasProperty(string key)
        {
            return properties.ContainsKey(key);
        }

        public int GetIntProperty(string key)
        {
            return HasProperty(key) ? (int)properties[key] : 0;
        }

        public string GetStringProperty(string key)
        {
            return HasProperty(key) ? (string)properties[key] : default;
        }

        public void SetIntProperty(string name, int value) => properties[name] = value;

        public void SetStringProperty(string name, string value) => properties[name] = value;

        public void RemoveProperty(string name) => properties.Remove(name);

        public void Clear() => properties = new Dictionary<string, object>();

        public void SaveToFile(string filename)
        {
            try
            {
                if (!Directory.Exists("saves"))
                    Directory.CreateDirectory("saves");

                if (File.Exists("saves/" + filename))
                    File.Delete("saves/" + filename);

                FileStream file = File.OpenWrite("saves/" + filename);

                foreach (string key in properties.Keys)
                {
                    byte[] encodedType = new byte[1];
                    byte[] encodedKey = Encoding.ASCII.GetBytes(key);
                    byte[] encodedKeyLength = BitConverter.GetBytes(encodedKey.Length);
                    byte[] encodedLength = null;
                    byte[] encodedObject = null;

                    if (properties[key].GetType() == typeof(int))
                    {
                        encodedType[0] = 0;
                        encodedObject = BitConverter.GetBytes((int)properties[key]);
                        encodedLength = BitConverter.GetBytes(sizeof(int));
                    }
                    else if (properties[key].GetType() == typeof(string))
                    {
                        encodedType[0] = 1;
                        encodedObject = Encoding.ASCII.GetBytes((string)properties[key]);
                        encodedLength = BitConverter.GetBytes(((string)properties[key]).Length);
                    }

                    file.Write(encodedType, 0, encodedType.Length);
                    file.Write(encodedKeyLength, 0, encodedKeyLength.Length);
                    file.Write(encodedKey, 0, encodedKey.Length);
                    file.Write(encodedLength, 0, encodedLength.Length);
                    file.Write(encodedObject, 0, encodedObject.Length);
                }

                file.Close();
            } catch (Exception e)
            {
                Debug.Error("writing saves/" + filename + " failed\n" + e.ToString());
            }
        }

        public void LoadFromFile(string filename)
        {
            if (File.Exists("saves/" + filename))
            {
                try
                {
                    FileStream file = File.OpenRead("saves/" + filename);

                    bool finished = false;

                    while (!finished)
                    {
                        try
                        {
                            byte[] type = new byte[1];
                            file.Read(type, 0, 1);

                            byte[] keyLength = new byte[4];
                            file.Read(keyLength, 0, 4);

                            int decodedKeyLength = BitConverter.ToInt32(keyLength, 0);
                            byte[] key = new byte[decodedKeyLength];
                            file.Read(key, 0, decodedKeyLength);

                            string decodedKey = Encoding.ASCII.GetString(key);

                            byte[] length = new byte[4];
                            file.Read(length, 0, 4);

                            int decodedLength = BitConverter.ToInt32(length, 0);
                            byte[] encodedObject = new byte[decodedLength];
                            file.Read(encodedObject, 0, decodedLength);

                            object value = null;

                            if (type[0] == 0)
                                value = BitConverter.ToInt32(encodedObject, 0);
                            else if (type[0] == 1)
                                value = Encoding.ASCII.GetString(encodedObject);

                            properties.Add(decodedKey, value);
                        }
                        catch { finished = true; }
                    }

                    file.Close();
                } catch (Exception e)
                {
                    Debug.Error("reading saves/" + filename + " failed\n" + e.ToString());
                    Clear();
                }
            }
        }
    }
}