using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Singleton
{
    public class Textures
    {
        public static Texture2D white;

        public static void Initialize(ref GraphicsDevice graphicsDevice)
        {
            white = new Texture2D(graphicsDevice, 1, 1);
            white.SetData(new Color[] { Color.White });
        }
    }

    public class GameObject
    {
        public Vector2 position = Vector2.Zero;
        public Rectangle collider = new Rectangle(0, 0, 32, 32);
        public Vector2 size = new Vector2(32, 32);
        public Color color = Color.White;
        public Texture2D texture;
        public Behavior behavior;

        public bool updatable = true;
        public bool visible = true;
        public bool collidable = true;

        public string ID = "";

        public void ReadyInput() => behavior.ReadyInput();
        
        public virtual void Update(GameTime gameTime) {}
        public virtual void Draw(ref SpriteBatch spriteBatch)
        {
            if (texture == null)
                spriteBatch.Draw(Textures.white,
                destinationRectangle: new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y),
                color: color);
            else
                spriteBatch.Draw(texture,
                destinationRectangle: new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y),
                color: color);
        }
    }
}